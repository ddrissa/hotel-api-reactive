package ci.kossovo.hotel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelApiReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelApiReactiveApplication.class, args);
	}

}
