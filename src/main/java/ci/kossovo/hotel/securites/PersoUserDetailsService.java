package ci.kossovo.hotel.securites;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import ci.kossovo.hotel.outils.ConstantSecurite;
import ci.kossovo.hotel.securites.model.UserRemonte;
import reactor.core.publisher.Mono;

@Service
public class PersoUserDetailsService {

	public Mono<UserPrincipal> loadUserById(Long id, String token) {
		String url = "http://localhost:8183";

		WebClient webClient = WebClient.create(url);

		return webClient.get().uri("/api/userremonte/" + id)
				.header(ConstantSecurite.TOKEN_HEADER, ConstantSecurite.TOKEN_PREFIX + token)
				.retrieve()
				.bodyToMono(UserRemonte.class)
				.map(userRe -> UserPrincipal.create(userRe, userRe.getRoles()));
	}

}
