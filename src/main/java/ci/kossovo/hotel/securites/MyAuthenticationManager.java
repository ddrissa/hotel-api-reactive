package ci.kossovo.hotel.securites;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Mono;

@Component
public class MyAuthenticationManager implements ReactiveAuthenticationManager {
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	@Autowired
	private PersoUserDetailsService persoUserDetailsService;

	@Override
	public Mono<Authentication> authenticate(Authentication authentication) {
		String jwt = authentication.getCredentials().toString();

		Long userId;
		try {
			userId = jwtTokenProvider.getUserIdFromJWT(jwt);
		} catch (Exception e) {
			userId = null;
		}

		if (userId != null && jwtTokenProvider.validateToken(jwt)) {

			Mono<Authentication> auth = persoUserDetailsService.loadUserById(userId, jwt)
					.map(details -> new UsernamePasswordAuthenticationToken(details, null, details.getAuthorities()));

			return auth;
		}

		return Mono.empty();
	}

}
