package ci.kossovo.hotel.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class EmployerController {
	
	@GetMapping("/employes")
	@PreAuthorize("hasRole('GERANT') AND hasRole('DBA')")
	public Mono<String> getEmploye() {
		return Mono.just(" Si vous voyez ce message, alors WebClient a marche!");
	}

}
