package ci.kossovo.hotel.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ClientController {
	
	@GetMapping("/clients")
	public Mono<String> getEmploye() {
		return Mono.just(" Si vous voyez ce message, alors client sans authorisation reactive marche!");
	}

}
