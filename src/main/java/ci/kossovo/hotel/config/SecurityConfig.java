package ci.kossovo.hotel.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

import ci.kossovo.hotel.securites.MyAuthenticationManager;
import ci.kossovo.hotel.securites.MySecurityContextRepository;



@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class SecurityConfig {

	@Autowired
	private MyAuthenticationManager authenticationManager;
	@Autowired
	private MySecurityContextRepository securityContextRepository;
	

	
	
	@Bean
	public SecurityWebFilterChain securitygWebFilterChain(ServerHttpSecurity http)  {

		return http.csrf().disable()
				.formLogin().disable()
				.httpBasic().disable()
				.authenticationManager(authenticationManager)
				.securityContextRepository(securityContextRepository)
				.authorizeExchange()
				.pathMatchers(HttpMethod.OPTIONS).permitAll()
				.pathMatchers("/api/clients/**").permitAll()
				.anyExchange().authenticated()
				.and().build();

	}
	
}
